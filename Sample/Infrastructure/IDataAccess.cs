﻿using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public interface IDataAccess
    {
       Task<IPageContent> GetPageContent();
    }
}
