﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain.Abstract;
using Domain.Entities;

namespace Infrastructure
{
    public class DataAccess : IDataAccess
    {
        private IPageContent helloWorldPageContent;
        public async Task<IPageContent> GetPageContent()
        {
           if (this.helloWorldPageContent == null)
            {
                this.helloWorldPageContent = new HelloWorldPageContent();
            } 
           return this.helloWorldPageContent;
        }
    }
}
