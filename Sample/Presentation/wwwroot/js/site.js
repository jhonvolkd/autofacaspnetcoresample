﻿// Write your Javascript code.
$(function () {
    console.log('onload');
    $.ajax({
        method: 'get',
        url: 'http://localhost:49784/api/values',
    })
        .done(function (msg) {
            console.log('done', msg);
            // concat the page content 10 times 
            var text = (new Array(10)).fill(msg.pageContent).join(' ');
            $('#content').text(text);
        })
        .fail(function (err) {
            console.log('fail', err);
        })
})