﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Infrastructure;
using Newtonsoft.Json.Linq;

namespace Sample.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private ILogger<ValuesController> _logger;
        public IDataAccess _dataAccess;

        public ValuesController(IDataAccess dataAccess, ILogger<ValuesController> logger)
        {
            _logger = logger;
            _dataAccess = dataAccess;
        }
        // GET api/values
        [HttpGet]
        public async Task<JObject> Get()
        {
            _logger.LogInformation("accessing Get()");
            // create json return object
            dynamic jsonData = new JObject();
            var pageContent = await _dataAccess.GetPageContent();
            jsonData.pageContent = await pageContent.GetData();
            return jsonData;
        }
    }
}
