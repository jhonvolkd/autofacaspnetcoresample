﻿using Autofac;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new DataAccess())
                .As<IDataAccess>()
                .InstancePerLifetimeScope();
        }

    }
}
