﻿using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class HelloWorldPageContent : IPageContent
    {
        public Task<string> GetData()
        {
            return Task<string>.Factory.StartNew(() => "Hello World");
        }
    }
}
